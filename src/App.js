import React, {useState, useEffect, createRef} from 'react';
import {fetchPokemonsFrom, fetchPokemonInfoFrom} from './services/DataFetcher'
import Card from './Card/Card'
import Pagination from './Pagination/Pagination'
import './App.css';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

function App() {
  const [pokemonInfo, setPokemonInfo] = useState([]);
  const [nextPageUrl, setNextPageUrl] = useState('');
  const [previousPageUrl, setPreviousPageUrl] = useState('');
  const [loading, setLoading] = useState(true);
  const [currentPageUrl, setCurrentPageUrl] = useState('https://pokeapi.co/api/v2/pokemon');

  useEffect(() => {
    async function fetchData() {
      //change to axios ?
      let response = await fetchPokemonsFrom(currentPageUrl);
      setNextPageUrl(response.next);
      setPreviousPageUrl(response.previous);
      let pokemon = await loadingPokemon(response.results);
      console.log(pokemon);
      setLoading(false);
    }

    fetchData(); 
  }, [currentPageUrl])

  createRef(() => {

  })

  const loadingPokemon = async (data) => {
    let _pokemonData = await Promise.all(data.map(async pokemon => {
      let pokemonRecord = await fetchPokemonInfoFrom(pokemon.url);
      return pokemonRecord
    }))
    setPokemonInfo(_pokemonData)

  }

  function goForward() {
    setCurrentPageUrl(nextPageUrl)
    window.scroll(0,0)
  }

  function goBackward() {
    setCurrentPageUrl(previousPageUrl)
    window.scroll(0,0)
  }

  return (
    <div >
      {loading ? <div className='loader_container'>
              <img className='loader' src={require('./pokeball.png')} alt='pokeball'/>
              <img src={require('./gotta_catch_em_all.png')} width={600} alt='gotta catch em all'/>
          </div> : (
        <>
          <div>
              <img className='logo' src={require('./pokemon_logo.png')} alt='pokemon logo'/>
          </div>
          <div className='grid-container'>
            {pokemonInfo.map((pokemon) => {
              return <Card key={pokemon.name} pokemon={pokemon}/>
            })} 
           
          </div>
          <Pagination 
                goForward={nextPageUrl ? goForward : null} 
                goBackward={previousPageUrl ? goBackward : null}/>
        </>
        )}
    </div>

  );
}

export default App;

