import React from 'react'
import './paginationStyle.css'

export default function Pagination({goForward, goBackward}) {
    return (
        <div className='pagination_container'>
            { goBackward && <button className='pagination_button pagination_left' onClick={goBackward}>Previous</button> }
            { goForward && <button className='pagination_button pagination_right' onClick={goForward}>Next</button> }
        </div>
    )
}
