import React from 'react'
import './cardStyle.css'


export default function Card({pokemon}) {
    return (
        <div className='card'>
            <div className='card_img'>
                <img src={pokemon.sprites.front_default} alt={pokemon.name} width="200"/>
            </div>
            <div className='card_name'>
                {pokemon.name}
            </div>
            <div className='card_types'>
                {
                    pokemon.types.map(type => {
                        return (
                            <div key={type.type.name} className='card_type'>
                                <p>{type.type.name}</p>
                            </div>
                        )
                    })
                }
            </div>
            <div className='card_info'>
                <div className='card_data card_data--weight'>
                    <p className='title'>Weight</p>
                    <p>{pokemon.weight}</p>
                </div>
                <div className='card_data card_data--weight'>
                    <p className='title'>Height</p>
                    <p>{pokemon.height}</p>
                </div>
                <div className='card_data card_data--ability'>
                    <p className='title'>Ability</p>
                    <p>{pokemon.abilities[0].ability.name}</p>
                </div>
                <div className='card_data card_data--ability'>
                    <p className='index_number'>#{pokemon.order}</p>
                </div>
            </div>
        </div> 
    )
}
